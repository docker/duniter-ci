FROM buildpack-deps:latest
MAINTAINER elois <c@elo.tf>

ENV DEBIAN_FRONTEND noninteractive
ENV NODEJS_VERSION 20.2.0

# Add cmake
RUN apt-get update; \
   apt-get install -y cmake
   
# Rust
Run curl -sSf https://static.rust-lang.org/rustup.sh | sh

# user
RUN useradd --create-home -s /bin/bash user
WORKDIR /home/user
ENV HOME /home/user
ENV USER user

# External scripts
ADD . /home/user
RUN chmod +x install_nodejs.sh

# Now act as `user`
USER user

# Install NodeJS
RUN ./install_nodejs.sh $NODEJS_VERSION
